cmake_minimum_required(VERSION 3.10)
project(DICE_DETECTION VERSION 0.1.0)


file(GLOB detectnetSources *.cpp)
file(GLOB detectnetIncludes *.h )

# import jetson-inference and jetson-utils packages.
# note that if you didn't do "sudo make install"
# while building jetson-inference, this will error.
find_package(jetson-utils)
find_package(jetson-inference)

# CUDA is required
find_package(CUDA)

# add directory for libnvbuf-utils to program
link_directories(/usr/lib/aarch64-linux-gnu/tegra)

# Add own library
add_library(myLIB src/functions.cpp)

add_library(mlLIB src/ml_utility.cpp)

# add_executable(main src/main.cpp)
cuda_add_executable(main src/main.cpp ${detectnetSources})

# Target all the libraries
target_link_libraries(main myLIB mlLIB jetson-inference)


# Specify the C++ standard
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED True)

# Make release the default build
if(NOT CMAKE_BUILD_TYPE)
	  set(CMAKE_BUILD_TYPE Release)
	    message(STATUS "Build type not specified: Use Release by default")
	endif()

	set(CMAKE_CXX_FLAGS "-Wall -Wextra")
	set(CMAKE_CXX_FLAGS_DEBUG "-g")
	set(CMAKE_CXX_FLAGS_RELEASE "-O3")

	message(STATUS "Build type: ${CMAKE_BUILD_TYPE}")