#ifndef mllib_LIB_H
#define mllib_LIB_H
#include <jetson-inference/imageNet.h>
#include <jetson-utils/loadImage.h>

#include <jetson-inference/detectNet.h>

#include "jetson-utils/videoSource.h"
#include "jetson-utils/videoOutput.h"
#include <signal.h>


int detect( int argc, char** argv );

#endif
