# DnD Dice Roll Recorder

## Table of Contents

- [Requirements](#requirements)
- [Description](#description)
- [Setup](#setup)
- [Usage](#usage)
- [Details](#details)
- [Output](#output)
- [Maintainers](#maintainers)
- [Contributers](#contributers)

## Description
The goal of this project is to construct a system for automatically recording dice rolls for Dungeon and Dragons games.

Rough sketch:
* Use a pretrained Object Detection Network (such as SSD-mobilenet) to detect the dice.
* Possible addition: move the robot arm if a die is rolling outside the view
* Ignore movement - wait for the dice to stop rolling before continuing
* For each detected object:
* - Detect the die type
* - Detect the rolled value
* Transmit the annotated video stream from the Nano to a pc

Solution:
* Create a network that can output a bounding box for each die
* Create a dataset of dice rolls
* Use the network to extract the bounding boxes
* The second network takes both the class (d6, d10, d20, etc.) and the view as input.

## Requirements

Software:
* Requires `gcc`, `cmake`, `jetson-inference`

Hardware:
* Jetson Nano or equivalent. 
* CSI Camera (USB Camera requires slight modifications) 

## Setup
Build the `jetson-inference` project: https://github.com/dusty-nv/jetson-inference/blob/master/docs/building-repo-2.md 

Physical setup:
* The trained network expects a white'ish background viewed directly from 15cm above. Slight deviations from this setup should still work.
* Roll/Put the dice within field of view.

## Usage
Training: 
* `cd jetson-inference`
* `./docker/run.sh`
* `cd jetson-inference/python/training/detection/ssd`
* `python3 train_ssd.py --dataset-type=voc --data=data/dice --model-dir=models/dice --epochs=500 --workers=1 --batch-size=2`
* `python3 onnx_export.py --model-dir=models/dice`

If continuing training, add `--resume=models/dice/<checkpoint.pth>`

The dataset can be requested by contacting Thorbjørn.

Testing:
* `mkdir build`
* `cd build`
* `cmake ../`
* `make`
* `./main --model=../net/ssd-mobilenet.onnx --labels=../net/labels.txt --input-blob=input_0 --output-cvg=scores --output-bbox=boxes csi://0`

Streaming using RTP: `--headless` and `rtp://<remote_IP:port>`

A guide on how to connect to the stream can be found here: https://github.com/dusty-nv/jetson-inference/blob/master/docs/aux-streaming.md#viewing-rtp-remotely 

## Details

Progress so far:
* Researched possible existing solutions. A ranked list is found below:
* 1. https://towardsdatascience.com/a-two-stage-stage-approach-to-counting-dice-values-with-tensorflow-and-pytorch-e5620e5fa0a3
* 2. https://github.com/nell-byler/dice_detection 
* 3. https://digitalcommons.wku.edu/cgi/viewcontent.cgi?article=1004&context=seas_faculty_pubs 
* To my surprise, none of these are full solutions to the detection problem. Solution 1 also applies a two-stage approach, however, it factors the problems differently. The outer network only locates the top of the dice, the inner network interprets the numbers. This split does not use the information from the shape of the die. As an example: a d6 can have a face value of 7 predicted.
* My next step was to find a good base for creating my own dice detector network. The Object Detection project from Jetson-Inference (https://github.com/dusty-nv/jetson-inference/blob/master/docs/pytorch-collect-detection.md) provides a good starting point. 
* I've used the `camera-capture` tool to create a custom data set and `train_ssd.py` for the actual training.  
* Using the tool, I've captured a dataset with equal amount of data for each class (d4, d6, d8, d10, d10p, d20), totalling 560 images. This dataset is only meant to illustrate the idea, a much larger set would be necessary for a deployable solution.
* The dice detector network was trained for 1000epochs ~ 30 hours of training on the Nano. This is a lot of training compared to what is used in the examples in the `jetson-inference` project. THis is due to the difficulty of the task - the difference for instance between a d10 and a d10p is only the numbers shown on the faces: (1,2,3,..,10) vs (00,10,20,..,90).
* After the training the network is able to confidently detect each type of die running at 35fps. Initial testing shows that transfer learning to another set of dice should be fairly easy - only a few images would be required for retraining.
* Streaming with RTP is a good solution for viewing the results in headless mode. Neither Remote Desktop nor graphical SSH comes close to the same performance, since they do not support EGL.
* Part two of the project requires more annotations of the data, a class (number) for each image. After that it is simply training an image classification network. Combining the two networks is easy using C++. My idea for handling the image classification would be to use a 7-channel input image - the only non-zero layer would be the die type. For instance channel 0 = d4, channel 1 = d6, etc.
* So far, I've not written much code in this project. It has more been a challenge in reading documentation, sourcing relevant methods and examples, choosing a good solution based on previous attempts and learning how to use the `jetson-inference` project to achieve real-time object detection in a headless setup. 

## Output
A video displaying the usage can be found here: https://photos.app.goo.gl/PwgLML4Zi4ax9rnd7

## Maintainers

[Thorbjørn (@Thunderl3ear)](https://gitlab.com/Thunderl3ear)

## Contributors and License
Copyright 2022,

[Thorbjørn (@Thunderl3ear)](https://gitlab.com/Thunderl3ear)
